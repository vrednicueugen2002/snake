const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const btn = document.getElementsByClassName('button');
const btnGO = document.getElementById('start');
const allbtns = document.getElementById('btns');
const score = document.getElementById('score');
const gamez = document.getElementById('game');

gamez.style.display = "none";


let dimensiune = "";

for (let i = 0; i < btn.length; i++)
    btn[i].addEventListener('click', () => {
        dimensiune = parseInt(btn[i].value);
    });

btnGO.addEventListener('click', () => {
    if (dimensiune != "") {
        allbtns.style.display = "none";
        gamez.style.display = "block";

        const coloane = canvas.width / dimensiune;
        let apple = {
            x: -1,
            y: -1
        }

        class Snake {
            constructor() {
                this.dim = 2;
                this.coord = [{ x: 0, y: Math.ceil((coloane / 2)) * dimensiune }, { x: dimensiune, y: Math.ceil((coloane / 2)) * dimensiune }];
            }

            draw() {
                this.coord.forEach((coordonata) => {
                    if (coordonata.x == this.coord[this.dim - 1].x && coordonata.y == this.coord[this.dim - 1].y) {
                        ctx.fillStyle = '#1363DF';
                    }
                    else ctx.fillStyle = '#2B7A0B';
                    ctx.beginPath();
                    ctx.arc(coordonata.x + dimensiune/2 , coordonata.y + dimensiune / 2, dimensiune/2, 0, 2 * Math.PI);
                    ctx.stroke();
                    ctx.fill();
                    //ctx.fillRect(coordonata.x, coordonata.y, dimensiune, dimensiune);
                });
            };

            eatApple() {
                let xa = this.coord[this.dim - 1].x;
                let ya = this.coord[this.dim - 1].y;
                if (xa == apple.x && ya == apple.y)
                    return true;
                return false;
            };

            move(direction) {
                let xa = this.coord[this.dim - 1].x;
                let ya = this.coord[this.dim - 1].y;
                let xtail = this.coord[0].x;
                let ytail = this.coord[0].y;
                for (let i = 0; i < this.dim - 1; i++)
                    this.coord[i] = this.coord[i + 1];
                let new_elem = {
                    x: xa,
                    y: ya
                }
                switch (direction) {
                    case "right": new_elem.x += dimensiune;
                        break;
                    case "up": new_elem.y -= dimensiune;
                        break;
                    case "left": new_elem.x -= dimensiune;
                        break;
                    case "down": new_elem.y += dimensiune;
                        break;
                }
                this.coord[this.dim - 1] = new_elem;
                if (this.eatApple() == true) {
                    let newBlock = {
                        x: xtail,
                        y: ytail
                    };
                    let block = JSON.parse(JSON.stringify(newBlock));
                    snake.coord.unshift(block);
                    snake.dim++;
                    apple.x = -1;
                    apple.y = -1;
                }
            };

            verify(direction) {
                switch (direction) {
                    case "right":
                        if (this.coord[this.dim - 1].x + dimensiune == this.coord[this.dim - 2].x)
                            return false;
                        break;
                    case "up":
                        if (this.coord[this.dim - 1].y - dimensiune == this.coord[this.dim - 2].y)
                            return false;
                        break;
                    case "left":
                        if (this.coord[this.dim - 1].x - dimensiune == this.coord[this.dim - 2].x)
                            return false;
                        break;
                    case "down":
                        if (this.coord[this.dim - 1].y + dimensiune == this.coord[this.dim - 2].y)
                            return false;
                        break;
                }
            };

            verify_state() {
                let first = JSON.parse(JSON.stringify(this.coord[this.dim - 1]));
                let checkstate = true;
                let i = 0;
                if (apple.x == -1)
                    i++;
                for (i; i < this.dim - 3; i++) {
                    if (first.x == this.coord[i].x && first.y == this.coord[i].y) {
                        checkstate = false;
                        i = this.dim;
                    }
                }
                return checkstate;
            };

            verify_map() {
                if (this.coord[this.dim - 1].x == 0 - dimensiune || this.coord[this.dim - 1].x == coloane * dimensiune || this.coord[this.dim - 1].y == 0 - dimensiune || this.coord[this.dim - 1].y == dimensiune * coloane)
                    return false;
                return true;
            };

            win() {
                if (this.dim == coloane * coloane)
                    return true;
                return false;
            };
        }

        let snake = new Snake();

        let posibilities = [];
        for (let i = 0; i < coloane; i++)
            posibilities[i] = [];
        for (let i = 0; i < coloane; i++)
            for (let j = 0; j < coloane; j++)
                posibilities[i][j] =
                {
                    x: i * dimensiune,
                    y: j * dimensiune
                };

        let arrayOfPosibilities = [];
        let m = 0;

        function initPos() {
            for (let i = 0; i < coloane; i++)
                for (let j = 0; j < coloane; j++)
                    if (posibilities[i][j].x != -1)
                        arrayOfPosibilities[m++] = posibilities[i][j];
            while (m > 0) {
                randomIndex = Math.floor(Math.random() * arrayOfPosibilities.length);
                let temp = arrayOfPosibilities[randomIndex];
                arrayOfPosibilities[randomIndex] = arrayOfPosibilities[m - 1];
                arrayOfPosibilities[m - 1] = temp;
                m--;
            }
        }

        initPos();

        function addApple() {
            let ok = true;
            for (let i = 0; i < arrayOfPosibilities.length; i++) {
                snake.coord.forEach((coordonata) => {
                    if (arrayOfPosibilities[i].x == coordonata.x && arrayOfPosibilities[i].y == coordonata.y)
                        ok = false;
                })
                if (ok == true) {
                    apple.x = arrayOfPosibilities[i].x;
                    apple.y = arrayOfPosibilities[i].y;
                    arrayOfPosibilities.splice(i, 1);
                    i = arrayOfPosibilities.length;
                }
                ok = true;
            }
            if (apple.x == -1 && snake.dim < coloane * coloane) {
                initPos();
                addApple();
            }
        }


        function draw() {
            ctx.fillStyle = '#E8A0BF';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            if (apple.x == -1)
                addApple();
            ctx.fillStyle = '#C21010';
            //ctx.fillRect(apple.x, apple.y, dimensiune, dimensiune);
            ctx.beginPath();
            ctx.arc(apple.x + dimensiune/2 , apple.y + dimensiune / 2, dimensiune/2, 0, 2 * Math.PI);
            ctx.stroke();
            ctx.fill();
        }

        draw();
        snake.draw();

        let olddirection = "";
        let direction = "right";

        function game() {
            if (snake.verify(direction) == false)
                direction = olddirection;
            snake.move(direction);
            if (!snake.verify_state() || snake.win() || !snake.verify_map()) {
                clearInterval(repetitie);
                window.location.reload();
            }
            else {
                draw();
                snake.draw();
                score.innerHTML = "Score: " + (snake.dim - 2);
            }
        }

        let repetitie = setInterval(game, 350);

        document.onkeydown = function (event) {
            switch (event.keyCode) {
                case 37:
                    olddirection = direction;
                    direction = "left";
                    break;
                case 38:
                    olddirection = direction;
                    direction = "up";
                    break;
                case 39:
                    olddirection = direction;
                    direction = "right";
                    break;
                case 40:
                    olddirection = direction;
                    direction = "down";
                    break;
            }
        };
    }
});
